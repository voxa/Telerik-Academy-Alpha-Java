package com.telerikacademy.springrestdemo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloWorldController {
    @GetMapping("/hello")
    public String sayHelloGet() {
        return "Hello World!";
    }

    @PostMapping("/hello")
    public String sayHelloPost() {
        return "Post Hello World!";
    }
}



// http://localhost:8080/api/hello
// http://localhost:8080 - server >>>>  404
//No message available