package com.telerikacademy.springrestdemo.controllers;

import com.telerikacademy.springrestdemo.models.Employee;
import com.telerikacademy.springrestdemo.models.Project;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/employees")
public class EmployeesController {

    private List<Employee> employees;

    public EmployeesController() {

        employees = new ArrayList<>();


        employees.add(new Employee(1, "Petar", "Petrov"));
        employees.add(new Employee(2, "Ivan", "Ivanov"));
        employees.add(new Employee(3, "Petar", "Ivanov"));
        employees.add(new Employee(4, "Iva", "Petiva"));

        Project math = new Project("Math");
        Project bio = new Project("Bio");

        Employee currEmployee1 = employees.get(0);
        currEmployee1.addProject(math);
        Employee currEmployee2 = employees.get(1);
        currEmployee2.addProject(bio);
        Employee currEmployee4 = employees.get(3);
        currEmployee4.addProject(math);
        currEmployee4.addProject(bio);

    }

    @GetMapping
    public List<Employee> getAll() {
        return employees;
    }

    @GetMapping(value = "searchByProject", params = "project")

    public List<Employee> searchByProject(@RequestParam("project") String projectName) {

        List<Employee> searchByProject = new ArrayList<>();
        for ( Employee e: employees ) {
            List<Project> temp = e.getProjects();

            for (Project p : temp) {

            if (p.getName().equals(projectName)){

                searchByProject.add(e);
            }
        }
        }

        if (searchByProject.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Employee with project %s was not found!", projectName));
        }
        return searchByProject;
    }


    @GetMapping(value = "search", params = "name")
    public List<Employee> searchByName(@RequestParam("name") String name) {

        List<Employee> searchByNameEmployee = employees
                .stream()
                .filter(x -> x.getFirstName().equals(name) ||
                        x.getLastName().equals(name) ||
                        (x.getFirstName() + x.getLastName()).equals(name))
                .collect(Collectors.toList());

        if (searchByNameEmployee.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Employee with name %s was not found!", name));
        }
        return searchByNameEmployee;
    }


    @GetMapping("/{id}")
    public Employee getById(@PathVariable int id) {
        return getByIdHelper(id);
    }

    @PostMapping("/new")
    public Employee create(@Valid @RequestBody Employee employee) {
        employees.add(employee);
        return employee;
    }

    @PutMapping("/{id}") //
    public Employee update(@PathVariable int id, @Valid @RequestBody Employee employee) {
        Employee employeeToUpdate = getByIdHelper(id);

        employeeToUpdate.setFirstName(employee.getFirstName());
        employeeToUpdate.setLastName(employee.getLastName());

        return employeeToUpdate;
    }

    private Employee getByIdHelper(int id) {
        return employees.stream()
                .filter(e -> e.getId() == id)
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Employee with id %d not found.", id)));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        Employee employeeToDelete = getByIdHelper(id);
        employees.remove(employeeToDelete);
    }
}
