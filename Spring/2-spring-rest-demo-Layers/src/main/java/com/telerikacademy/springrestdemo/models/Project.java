package com.telerikacademy.springrestdemo.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.ArrayList;
import java.util.List;


@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Project {

    private int id;
    private String name;
 //   @JsonBackReference
    private List<Employee> employeeList;

    public Project() {
        this.employeeList= new ArrayList<>();
    }

    public Project(int id, String name) {
        this.id = id;
        this.name = name;
        this.employeeList= new ArrayList<>();
    }

   // @JsonIgnore
    public List<Employee> getEmployees() {
        return employeeList;
    }

    public void addUser(Employee employee) {
        this.employeeList.add(employee);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
