package com.telerikacademy.springrestdemo.services;

import com.telerikacademy.springrestdemo.models.Employee;
import com.telerikacademy.springrestdemo.models.Project;
import com.telerikacademy.springrestdemo.repositories.RepositoryDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements ProjectsService {
    private RepositoryDB repository;

    @Autowired
    public ProjectServiceImpl(RepositoryDB repository) {
        this.repository = repository;
    }

    @Override
    public List<Project> getAllProjects() {
        return repository.getAllProjects();
    }

    @Override
    public Project getByName(String name) {
        return repository.getAllProjects()
                .stream()
                .filter(pr -> pr.getName().equals(name))
                .findFirst()
                .orElseThrow(
                        () -> new IllegalArgumentException(
                                String.format("Project with name %S not found.", name)));
    }

    @Override
    public void createProject(Project project) {
        List<Project> projects = repository
                .getAllProjects()
                .stream()
                .filter(e -> e.getName().equals(project.getName()))
                .collect(Collectors.toList());

        if (projects.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Project with name %s already exists", project.getName()));
        }

        repository.createProject(project);
    }


}
