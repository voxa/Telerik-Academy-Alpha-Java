package com.telerikacademy.springrestdemo.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


 @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Employee {
    private int id;

    @Size(min = 3, max = 20, message = "firstName must be between 3 and 20 symbols.")
    private String firstName;

    @Size(min = 5, max = 25, message = "lastName must be between 5 and 25 symbols.")
    private String lastName;

    private List<Project> projectList;

    public Employee() {
       this.projectList = new ArrayList<>();
    }

    public Employee(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.projectList = new ArrayList<>();
    }


 //  @JsonIgnore
 // @JsonIgnoreProperties("projects")
    public List<Project> getProjects() {
        return projectList;
    }

    public void addProject(Project project) {
        this.projectList.add(project);
    }

    public void removeProject(Project project) {
        this.projectList.remove(project);
    }


    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


}
