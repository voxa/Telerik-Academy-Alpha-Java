package com.telerikacademy.springrestdemo.repositories;


import com.telerikacademy.springrestdemo.models.Employee;
import com.telerikacademy.springrestdemo.models.Project;
import org.springframework.stereotype.Repository;


import java.util.*;

@Repository // da ne zabravim anotaciqta
public class RepositoryDBImpl implements RepositoryDB {

    private List<Employee> employees;
    private List<Project> projects;


    public RepositoryDBImpl() {

        employees = new ArrayList<>();
        projects = new ArrayList<>();


        Project math = new Project(1, "Math");
        Project bio = new Project(2, "Bio");
        Project history = new Project(3, "History");
        projects.add(math);
        projects.add(bio);
        projects.add(history);

        Employee e1 = new Employee(1, "Ivan", "Ivanov");
        e1.addProject(math);
        math.addUser(e1);
        employees.add(e1);

        Employee e2 = new Employee(2, "Petar", "Ivanov");
        e2.addProject(bio);
        bio.addUser(e2);
        employees.add(e2);

        Employee e3 = new Employee(3, "Iva", "Petiva");
        e3.addProject(history);
        history.addUser(e3);
        e3.addProject(bio);
        bio.addUser(e3);
        employees.add(e3);


    }

    @Override
    public List<Employee> getAllEmployee() {
        return employees;
    }

    @Override
    public List<Project> getAllProjects() {
        return projects;
    }

    @Override
    public void createEmployee(Employee employee) {
        employees.add(employee);
    }


    @Override
    public void createProject(Project project) {
        projects.add(project);
    }
}
