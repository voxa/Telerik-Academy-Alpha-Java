package com.telerikacademy.springrestdemo.controllers;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.telerikacademy.springrestdemo.models.Employee;
import com.telerikacademy.springrestdemo.models.Project;
import com.telerikacademy.springrestdemo.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {
    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {

        this.employeeService = employeeService;
    }

    @GetMapping

    public List<Employee> getAll() {
        return employeeService.getAllEmployee();
    }

    @GetMapping(value = "searchByProject", params = "project")

    public List<Employee> searchByProject(@RequestParam("project") String projectName) {

        List<Employee> searchByProject = new ArrayList<>();
        for (Employee e : employeeService.getAllEmployee()) {
            List<Project> temp = e.getProjects();
            for (Project p : temp) {
                if (p.getName().equals(projectName)) {
                    searchByProject.add(e);
                }
            }
        }

        if (searchByProject.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Employee with project %s was not found!", projectName));
        }
        return searchByProject;
    }


    @GetMapping(value = "search", params = "name")
    public List<Employee> searchByName(@RequestParam("name") String name) {

        List<Employee> searchByNameEmployee = employeeService
                .getAllEmployee()
                .stream()
                .filter(x -> x.getFirstName().equals(name) ||
                        x.getLastName().equals(name) ||
                        (x.getFirstName() + " " + x.getLastName()).equals(name))
                .collect(Collectors.toList());

        if (searchByNameEmployee.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Employee with name %s was not found!", name));
        }
        return searchByNameEmployee;
    }

    @GetMapping("/{id}")
    public Employee getById(@PathVariable int id) {

        try {
            return employeeService.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PostMapping("/new")
    public Employee createEmployee(@RequestBody Employee employee) {
        try {
            employeeService.createEmployee(employee);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return employee;
    }

    @PutMapping("/update/{id}") //
    public Employee update(@PathVariable int id, @RequestBody Employee employee) {
        Employee employeeToUpdate = getByIdHelper(id);

        employeeToUpdate.setFirstName(employee.getFirstName());
        employeeToUpdate.setLastName(employee.getLastName());

        return employeeToUpdate;
    }

    private Employee getByIdHelper(int id) {
        return employeeService.getAllEmployee().stream()
                .filter(e -> e.getId() == id)
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Employee with id %d not found.", id)));
    }

    @DeleteMapping("/{id}")
    public Employee delete(@PathVariable int id) {
        Employee employeeToDelete = getByIdHelper(id);

        employeeService.getAllEmployee().remove(employeeToDelete);

        // za da sme sigurni che e iztrit , trqbwa da vrushta iztritiq employee
        return employeeToDelete;
    }
}
