package com.telerikacademy.springrestdemo.services;


import com.telerikacademy.springrestdemo.models.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getAllEmployee();
    Employee getById(int id);
    void createEmployee(Employee employee);
}
