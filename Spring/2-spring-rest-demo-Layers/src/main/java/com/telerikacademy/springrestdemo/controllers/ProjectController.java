package com.telerikacademy.springrestdemo.controllers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.telerikacademy.springrestdemo.models.Employee;
import com.telerikacademy.springrestdemo.models.Project;
import com.telerikacademy.springrestdemo.repositories.RepositoryDBImpl;
import com.telerikacademy.springrestdemo.services.EmployeeService;
import com.telerikacademy.springrestdemo.services.ProjectsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("/api/projects")
public class ProjectController {
    private ProjectsService projectService;
    private EmployeeService employeeService;


    @Autowired
    public ProjectController(ProjectsService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    //@JsonIgnoreProperties("project")
    public List<Project> getAll() {
        return projectService.getAllProjects();
    }

    @GetMapping(value = "search", params = "name")
    public Project getByName(@RequestParam("name") String name) {

        try {
            return projectService.getByName(name);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/new")
    public Project createProject(@RequestBody Project project) {
        try {
            projectService.createProject(project);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return project;
    }

    private Project getByNameHelper(String name) {
        return projectService.getAllProjects().stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Project with name %s not found.", name)));
    }

    @DeleteMapping("/{name}") // ako iskame konkretnata stojnost polzwame {} !!!!!
    public Project delete(@PathVariable String name) {

        Project projectToDelete = getByNameHelper(name);

        projectService.getAllProjects().remove(projectToDelete);
        return projectToDelete;
    }
}
