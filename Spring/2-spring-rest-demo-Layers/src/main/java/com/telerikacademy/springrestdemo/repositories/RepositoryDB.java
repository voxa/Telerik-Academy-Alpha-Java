package com.telerikacademy.springrestdemo.repositories;


import com.telerikacademy.springrestdemo.models.Employee;
import com.telerikacademy.springrestdemo.models.Project;

import java.util.List;

public interface RepositoryDB {

    List<Employee> getAllEmployee();

    List<Project> getAllProjects();

    void createEmployee(Employee employee);

    void createProject(Project project);
}
