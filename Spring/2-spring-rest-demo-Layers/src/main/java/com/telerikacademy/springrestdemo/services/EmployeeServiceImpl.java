package com.telerikacademy.springrestdemo.services;

import com.telerikacademy.springrestdemo.models.Employee;


import com.telerikacademy.springrestdemo.repositories.RepositoryDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private RepositoryDB repository;


    @Autowired
    public EmployeeServiceImpl(RepositoryDB repository) {
        this.repository = repository;
    }

    @Override
    public List<Employee> getAllEmployee() {
        return repository.getAllEmployee();
    }

    @Override
    public Employee getById(int id) {
        return repository.getAllEmployee().stream()
                .filter(employee -> employee.getId() == id)
                .findFirst()
                .orElseThrow(
                        () -> new IllegalArgumentException(
                                String.format("Employee with id %d not found.", id)));
    }


    @Override
    public void createEmployee(Employee employee) {
        List<Employee> employees = repository
                .getAllEmployee()
                .stream()
                .filter(e -> e.getFirstName().equals(employee.getFirstName()) &&
                        e.getLastName().equals(employee.getLastName()) ||
                        e.getId() == employee.getId()
                )
                .collect(Collectors.toList());

        if (employees.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Employee with name %s %s  or id %d already exists", employee.getFirstName(), employee.getLastName() , employee.getId()));
        }

        repository.createEmployee(employee);
    }
}
