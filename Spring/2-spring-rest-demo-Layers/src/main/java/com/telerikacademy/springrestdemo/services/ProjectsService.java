package com.telerikacademy.springrestdemo.services;



import com.telerikacademy.springrestdemo.models.Project;

import java.util.List;

public interface ProjectsService {

    List<Project> getAllProjects();

    Project getByName(String name);

    void createProject(Project project);
}
