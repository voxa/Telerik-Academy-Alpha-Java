package Recursion;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Numerology {


    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        String data = sc.nextLine();

        List<Integer> birthday = new ArrayList<>(); // 8 obshto

        for (int i = 0; i < data.length(); i++) {
            birthday.add(data.charAt(i) - '0');
        }

        int[] digitsCount = new int[10]; // 0-9

        getNum(birthday, digitsCount);

        for (int i = 0; i <= 9; i++) {
            System.out.print(digitsCount[i] + " ");
        }
    }

    private static int[] getNum(List<Integer> birthday, int[] digitsCount) {

        int a = 0;
        int b = 0;
        int result = 0;

        if (birthday.size() == 1) {

            int digit = birthday.get(0);
            digitsCount[digit] = digitsCount[digit] + 1;
            return digitsCount;

        }

        for (int i = 0; i < birthday.size() - 1; i++) { // da ne izlezem

            a = birthday.get(i);
            b = birthday.get(i + 1);
            result = (a + b) * (a ^ b) % 10;

            ArrayList<Integer> newBirthday = new ArrayList<>(birthday);
            //   System.out.println(newBirthday.toString());

            newBirthday.remove(i);
            newBirthday.set(i, result);

            //    System.out.println(newBirthday.toString());

            getNum(newBirthday, digitsCount);
        }
        return digitsCount;
    }
}