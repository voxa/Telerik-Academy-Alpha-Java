package Recursion;

import java.util.Scanner;

public class RecursionPath05Triangle {

    public static void main(String[] args) {

        // We have triangle made of blocks. The topmost row has 1 block,
        // the next row down has 2 blocks, the next row has 3 blocks, and so on.
        // Compute recursively (no loops or multiplication) the total number
        // of blocks in such a triangle with the given number of rows.

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        System.out.println(triangleBlocks(n));

    }

    static int triangleBlocks(int n) {

        if (n == 0) {
            return 0;
        }

        return n + triangleBlocks(n - 1);

    }

}

