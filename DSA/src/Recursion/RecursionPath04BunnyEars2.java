package Recursion;

import java.util.Scanner;

public class RecursionPath04BunnyEars2 {

    public static void main(String[] args) {

        // We have bunnies standing in a line, numbered 1, 2, ...
        // The odd bunnies (1, 3, ..) have the normal 2 ears.
        //The even bunnies (2, 4, ..) we'll say have 3 ears,
        // because they each have a raised foot.
        // Recursively return the number of "ears" in the
        // bunny line 1, 2, ... n (without loops or multiplication).

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        System.out.println(bunnyEars(n));

    }


    static int bunnyEars(int n) {

        int ears = 0;

        if (n == 0) {
            return 0;
        }

        if (n % 2 == 0) {
            ears = 3;
        } else {
            ears = 2;
        }

        return ears + bunnyEars(n - 1);

    }

}

