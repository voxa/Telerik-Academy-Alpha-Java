package Recursion;

import java.util.Scanner;

public class RecursionPath13ChangePi {

    public static void main(String[] args) {

        // Given a string, compute recursively (no loops) a new
        // string where all appearances of "pi" have been replaced by "3.14".

        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();

        System.out.println(changePi(word));

    }

    private static String changePi(String word) {

        if (word.contains("pi")) {

            if (word.substring(word.length() - 2).equals("pi")) {
                word = word.replace("pi", "3.14"); // !!! a ne da reja
                return changePi(word);
            }

            return changePi(word.substring(0, word.length() - 1)) + word.charAt(word.length() - 1); // да го прибавя отзад, за да не изчезва

        }
        return word;
    }

}






