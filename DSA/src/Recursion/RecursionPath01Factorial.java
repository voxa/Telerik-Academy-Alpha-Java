package Recursion;

import java.util.Scanner;

public class RecursionPath01Factorial {

    public static void main(String[] args) {


        // Given n of 1 or more, return the factorial of n, which is n (n-1) (n-2) ... 1.
        // //Compute the result recursively (without loops).

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        System.out.println(factorial(n));
    }

    static int factorial(int n) {

        if (n < 3) {
            return n;
        }

        return  n * factorial(n-1);

    }

}



