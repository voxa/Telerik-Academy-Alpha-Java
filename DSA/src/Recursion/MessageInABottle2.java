package Recursion;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class MessageInABottle2 {

    private static int count = 0;
    private static HashMap<String,Character> map = new HashMap<>();
    private static Set<String> alpha = new TreeSet<>();

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        String digits = sc.nextLine();
        String code = sc.nextLine();

        String[] arrDigitsPattern = code.substring(1).split("[A-Z]");
        String onlyLetters = code.replaceAll("[0-9]", "");

        for (int i = 0; i < arrDigitsPattern.length; i++) {
            map.put(arrDigitsPattern[i],onlyLetters.charAt(i));
        }

        getCodeStr(digits, "");

        System.out.println(count);
        alpha.forEach(System.out::println);

    }


    private static void getCodeStr(String digits, String result) {

        if (digits.isEmpty()) { // tupik
            count++;
            alpha.add(result);
            return;
        }

        String num ="";

        for (int j = 0; j < digits.length(); j++) {

           num += String.valueOf(digits.charAt(j));

                if(map.containsKey(num)) {
                    getCodeStr(digits.substring(j + 1),  result + map.get(num));
                }
            }
    }
}