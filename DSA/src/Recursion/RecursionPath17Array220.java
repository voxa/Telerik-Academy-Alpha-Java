package Recursion;

import java.util.Scanner;

public class RecursionPath17Array220 {

    public static void main(String[] args) {

        // Given an array of ints, compute recursively if the array contains
        //  somewhere a value followed in the array by that value times 10.
        //We'll use the convention of considering only the part of the array
        // that begins at the given index. In this way, a recursive call can
        // pass index+1 to move down the array. The initial call will pass in index as 0.

        Scanner sc = new Scanner(System.in);

        String[] arr = sc.nextLine().split(",");
        int index = Integer.parseInt(sc.nextLine());

        System.out.println(аrray220(arr, index));

    }


    private static boolean аrray220(String[] arr, int index) {

        if (index == arr.length) {
            return false;
        }
String current = arr[index];
// arr[index]
        if (current.charAt(current.length()-1)== '0'&& current.charAt(current.length()-2) !=' '){
            return true;
        }

        return  аrray220(arr, index + 1);
    }

}






