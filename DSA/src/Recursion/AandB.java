package Recursion;


import java.util.Scanner;

public class AandB {

    private static StringBuilder sb = new StringBuilder();

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        int n = Integer.parseInt(sc.nextLine());
        int x = Integer.parseInt(sc.next());
        int y = Integer.parseInt(sc.next());

        String input = "";

        if (x < y) {
            input = x + "" + y;

        } else {
            input = y + "" + x;
        }

        String prefix = "";

        getCombinations(n, input, prefix);

        System.out.println(sb.toString());
    }


    private static void getCombinations(int n, String input, String prefix) {

        if (n == 0) {
            sb.append(prefix).append("\n");
            return;
        }
        getCombinations(n - 1, input, prefix + input.charAt(0));
        getCombinations(n - 1, input, prefix + input.charAt(1));

    }

}