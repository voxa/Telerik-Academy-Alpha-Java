package Recursion;

import java.util.Scanner;

public class RecursionPath09PowerN {

    public static void main(String[] args) {

        // On the first line you will be given the base(b). On the second line you will be given the power(n).
        //Output
        //
        //On the only output line you should print the result of b to the power of n.

        Scanner sc = new Scanner(System.in);

        int num = Integer.parseInt(sc.nextLine());
        int n = Integer.parseInt(sc.nextLine());

        System.out.println(powerN(num, n));

    }


    static int powerN(int num, int n) {

        if (n == 0) {// duno
            return 1;
        }
               return num * powerN(num, n - 1);
    }

}

