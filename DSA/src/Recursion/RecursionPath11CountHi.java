package Recursion;

import java.util.Scanner;

public class RecursionPath11CountHi {

    public static void main(String[] args) {

        // On the first line you will be given a string
        //Output
        //On the only output line you should print the number of hi in it.
// xxhixx

        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();

        System.out.println(countHi(word));

    }

    private static int countHi(String word) {


            if(word.length() < 2) {
                return 0; // nqma nachin da ima hi
            } else {

                if(word.endsWith("hi")) {

                    return 1 + countHi(word.substring(0, word.length()-2));
                } else {

                    return countHi(word.substring(0, word.length()-1));
                }

            }
        }

    }



