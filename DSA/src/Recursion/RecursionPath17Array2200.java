package Recursion;

import java.util.Scanner;

public class RecursionPath17Array2200 {

    public static void main(String[] args) {

        // Given an array of ints, compute recursively if the array contains
        //  somewhere a value followed in the array by that value times 10.
        //We'll use the convention of considering only the part of the array
        // that begins at the given index. In this way, a recursive call can
        // pass index+1 to move down the array. The initial call will pass in index as 0.

        Scanner sc = new Scanner(System.in);

        String[] array = sc.nextLine().split(",");

        int []arr = new int[array.length];

        for(int i=0; i<array.length; i++) {

            arr[i] = Integer.parseInt(array[i]);
        }

        int index = Integer.parseInt(sc.nextLine());

        System.out.println(аrray220(arr, index));

    }


    private static boolean аrray220(int[] arr, int index) {

        if (arr.length < 2) {
            return false;
        }

        if (arr.length == 2) {
            if (arr[1] == arr[0] * 10) {
                return true;
            } else {
                return false;
            }
        }

        if (index == arr.length - 1) {
            if (arr[arr.length - 1] != arr[arr.length - 2] * 10) {
                return false;
            } else {
                return true;
            }
        }

        if (arr[index + 1] == arr[index] * 10) {
            return true;
        }

            return аrray220(arr, index + 1);


    }

}




