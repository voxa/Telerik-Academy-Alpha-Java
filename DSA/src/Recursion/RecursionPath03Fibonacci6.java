package Recursion;

import java.util.Scanner;


public class RecursionPath03Fibonacci6 {


    public static void main(String args[]) {

        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        System.out.println(fibonacci(n));
    }

   private static long fibonacci(int n){
        return fibonacci(n, new long[n+1]);
    }

    private static  long fibonacci(int n, long[] memo) {

        if(n == 0 || n == 1) {
            return n;
        }
        if(memo[n] == 0){
            memo[n] = fibonacci(n - 1, memo) + fibonacci(n - 2, memo);
        }
        return  memo [n];
    }

}