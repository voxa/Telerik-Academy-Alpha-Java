package Recursion;

import java.util.Scanner;

public class RecursionPath06SumDigits {

    public static void main(String[] args) {

        // Given a non-negative int n, return the sum of its digits recursively (no loops).
        //Note that mod (%) by 10 yields the rightmost digit (126 % 10 is 6),
        // while divide (/) by 10 removes the rightmost digit (126 / 10 is 12).
        //Input
        //On the first line you will be given n.
        //Output
        //On the only output line you should print the sum.
        //Constraints
        //n>=0.

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        System.out.println(sumDigits(n));

    }

    static int sumDigits(int n) {

        if (n == 0) {
            return 0;
        }

        return  n % 10 + sumDigits(n / 10);

    }

}

