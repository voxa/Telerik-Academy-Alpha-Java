package Recursion;

import java.util.Scanner;

public class RecursionPath10CountX {

    public static void main(String[] args) {

        // On the first line you will be given a string
        //Output
            //On the only output line you should print the number of lowercase x in it.
// xxhixx


        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();

        System.out.println(countX(word));
    }

        static int countX (String word) {

        int count =0;

        if (word.length()>0) {
            if(word.substring(0,1).equals("x")) {
              count++;
            }
        }else {
            return count;
        }

 word = word.substring(1,word.length());
count += countX(word);
        return count ;

    }

}

