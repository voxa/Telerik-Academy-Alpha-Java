package Recursion;

import java.util.Scanner;

public class RecursionPath15Array6 {

    public static void main(String[] args) {

        // Given an array of ints, compute recursively if the array contains a 6.
        //We'll use the convention of considering only the part of the array that
        // begins at the given index. In this way, a recursive call can
        // pass index+1 to move down the array. The initial call will pass in index as 0.

        Scanner sc = new Scanner(System.in);

        String[] arr = sc.nextLine().split(",");
        int index = Integer.parseInt(sc.nextLine());

        System.out.println(аrray6(arr, index));

    }


    private static boolean аrray6(String[] arr, int index) {

        if(index==arr.length) {
            return false;
        }

        if (arr[index].equals("6")) {
            return true;
        }

        return аrray6(arr,index+1);
    }

}






