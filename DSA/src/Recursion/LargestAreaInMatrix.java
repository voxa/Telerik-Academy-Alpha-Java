package Recursion;

import java.util.Scanner;

public class LargestAreaInMatrix {

    private static boolean[][] visited ;
    private static String[][] matrix;
    private static int count=0;

    public static void main(String[] args) {

        // 5 6
        //1 3 2 2 2 4
        //3 3 3 2 4 4
        //4 3 1 2 3 3
        //4 3 1 3 3 1
        //4 3 3 3 1 1

        Scanner sc = new Scanner(System.in);
        String [] input = sc.nextLine().split(" ");
        int row = Integer.parseInt(input[0]);
        int col =  Integer.parseInt(input[1]);

        matrix =new String[row][col];

        for (int i = 0; i < row ; i++) {
            String[] line = sc.nextLine().split(" ");
            for (int j = 0; j < col ; j++) {
                matrix [i][j] =  line[j];
            }
        }

        visited = new boolean[row][col];
      //  int counter=0;
        int maxCounter=0;


        for (int i = 0; i < row ; i++) {
            for (int j = 0; j < col ; j++) {
             String current = matrix [i][j];
          count=0;
                findLargestArea( matrix,i,  j,current);
                if (maxCounter < count) {
                    maxCounter = count;

                }
            }

        }
        System.out.println(maxCounter);

    }
// public static void main(String[] args) {
//
//    int[][] foo = new int[][] {
//        new int[] { 1, 2, 3 },
//        new int[] { 1, 2, 3, 4},
//    };
//
//    System.out.println(foo.length); //2
//    System.out.println(foo[0].length); //3
//    System.out.println(foo[1].length); //4
//}


    private static void  findLargestArea(String [][] matrix, int row, int col,String current ) {


        if (row < 0 || col < 0 || row > matrix.length-1  || col > matrix[0].length-1 ) {
            return;
        }

        if (visited[row][col]) {
            return;
        }

        if (!matrix[row][col].equals( current)) {
            return;
        }

        visited[row][col] = true;
        count++;


        findLargestArea(matrix,row , col+1,current);
        findLargestArea(matrix, row+1, col,current);
        findLargestArea(matrix, row-1, col,current);
        findLargestArea(matrix, row, col-1,current);

    }

}
