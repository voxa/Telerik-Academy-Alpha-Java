package Recursion;

import java.util.Scanner;

public class RecursionPath08Count8 {

    public static void main(String[] args) {

        // Given a non-negative int n, return the count of the
        // occurrences of 7 as a digit, so for example 717 yields 2. (no loops).

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        System.out.println(count8(n));

    }


    static int count8(int n) {

        int count=0;

        if (n%10==8) {
            int m = n/10;
            if(m%10==8){
                count++;
            }
            count++;
        }

        if(n/10==0){
            return count; // towa e dunoto
        }

        return   count + count8(n / 10);
    }
}

