package Recursion;

import java.util.Scanner;

public class RecursionPath16Array11 {

    public static void main(String[] args) {

        // Given an array of ints, compute recursively the number
        // of times that the value 11 appears in the array.
        //We'll use the convention of considering only the part of the array
        // that begins at the given index. In this way, a recursive call can
        // pass index+1 to move down the array.
        // The initial call will pass in index as 0..

        Scanner sc = new Scanner(System.in);

        String[] arr = sc.nextLine().split(",");
        int index = Integer.parseInt(sc.nextLine());

        System.out.println(аrray11(arr, index));

    }


    private static int аrray11(String[] arr, int index) {

        int count = 0;

        if (index == arr.length) {
            return count;
        }

        if (arr[index].equals("11")) {
            count++;
        }

        return count + аrray11(arr, index + 1);
    }

}






