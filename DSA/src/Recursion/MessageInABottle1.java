package Recursion;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class MessageInABottle1 {

    private static Set<String> alpha = new TreeSet<>();
     private static String[] arrDigitsPattern;
    private static String onlyLetters;
    private static int count ;

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        String digits = sc.nextLine();
        String code = sc.nextLine();

        arrDigitsPattern = code.substring(1).split("[A-Z]");
        onlyLetters = code.replaceAll("[0-9]", "");

        getCodeStr(digits, "");

        System.out.println(count);
        alpha.forEach(System.out::println);

    }


    private static void getCodeStr(String digits, String result) {

        if (digits.isEmpty()) { // tupik
            count++;
            alpha.add(result);
            return;
        }

        StringBuilder sbTemp = new StringBuilder();

        for (int j = 0; j < digits.length(); j++) {
            sbTemp.append(digits.charAt(j));

            for (int i = 0; i < arrDigitsPattern.length; i++) {
                String number = arrDigitsPattern[i];

                if (number.equals(sbTemp.toString())
                        && !sbTemp.toString().isEmpty()) {

                    getCodeStr(digits.substring(j + 1),
                            result + onlyLetters.charAt(i));
                }
            }
        }
    }
}