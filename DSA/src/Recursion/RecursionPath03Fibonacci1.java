package Recursion;

import java.util.HashMap;
import java.util.Scanner;

public class RecursionPath03Fibonacci1 {

    public static void main(String[] args) {

        // 0, 1, 1, 2, 3, 5, 8, 13, 21
        // Hint: F(N) = F(N-1) + F(N-2)

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        System.out.println(fib(n));
    }

    static int fib(int n) {

        if (n < 2) {
            return n;
        }
        return fib(n - 1) + fib(n - 2);
    }
}
// fibonacci(5) = fibonacci(4) + fibonacci(3)
//
//fibonacci(3) = fibonacci(2) + fibonacci(1)
//
//fibonacci(4) = fibonacci(3) + fibonacci(2)
//
//fibonacci(2) = fibonacci(1) + fibonacci(0)




