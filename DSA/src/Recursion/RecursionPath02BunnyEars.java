package Recursion;

import java.util.Scanner;

public class RecursionPath02BunnyEars {

    public static void main(String[] args) {


        // On the first line you will be given the number of bunnies.
        //On the only output line you should print the number of their ears.

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        System.out.println(bunnyEars(n));
    }

    static int bunnyEars(int n) {


        if (n == 0) {
            return 0;
        }

        return 2 + bunnyEars(n - 1);

    }
}

