package HashMapTask_Telerik;

import java.util.Scanner;
import java.util.stream.IntStream;

public class AllocateArray {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        IntStream.range(0, n)
                .map(x -> x * 5)
                .forEach(System.out::println);



        int arraySize = Integer.parseInt(sc.nextLine()); // chetem obshtiq broj

        int [] numbers = new int [arraySize];

        for (int i = 0; i < arraySize; i++) { // tuk chetem wseki edin

            numbers[i] = i*5; // chetem pyrwoto chislo

            System.out.println(numbers[i]);
        }


    }
}