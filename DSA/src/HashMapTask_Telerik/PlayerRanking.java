package HashMapTask_Telerik;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

public class PlayerRanking {

    public static class Player implements Comparable<Player> {

        private String name="";
        private String type="";
        private int age=0;

       Player(String name, String type, int age) {
            this.name = name;
            this.type = type;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

         int getAge() {
            return age;
        }

        @Override
        public String toString() {
            return String.format("%s(%d)", this.getName(), this.getAge());
        }

        @Override
        public int compareTo(Player player) {

            int compareNames = this.getName().compareTo(player.getName());
            if (compareNames == 0) {
                compareNames = Integer.compare(this.getAge(), player.getAge());
                return -compareNames;
            }
            return compareNames;
        }
    }

    private static ArrayList<Player> ranklist = new ArrayList<>();
    private static HashMap<String, TreeSet<Player>> sortedByType = new HashMap<>();

    private static StringBuilder sb = new StringBuilder();

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String inputLine = br.readLine();


        String name = "";


        while (!inputLine.equals("end")) {

            String[] input = inputLine.split(" ");
            String command = input[0];

            switch (command) {
                case "add":
                    name = input[1];
                    String type = input[2];
                    int age = Integer.parseInt(input[3]);
                    int position = Integer.parseInt(input[4]);
                    add(name, type, age, position);
                    break;

                case "find":
                    name = input[1];
                    find(name);
                    break;

                case "ranklist":
                    int start = Integer.parseInt(input[1]);
                    int end = Integer.parseInt(input[2]);
                    rankList(start, end);
                    break;

            }

            inputLine = br.readLine();

        }
        br.close();
        System.out.println(sb.toString());


    }


    private static void find(String name) {

        sb.append(String.format("Type %s: ", name));

        if (sortedByType.containsKey(name)) {

            sb.append(
                    sortedByType
                            .get(name)
                            .stream()
                            .limit(5)
                            .map(Player::toString)
                            .collect(Collectors.joining("; ")));
        }

        sb.append("\n");
    }


    private static void rankList(int start, int end) {


        for (int i = start; i <= end; i++) {

            if (ranklist.size() >= i) {


                if (i == end) {

                    sb.append(String.format("%d. %s", i, ranklist.get(i - 1).toString()));
                } else {

                    sb.append(String.format("%d. %s; ", i, ranklist.get(i - 1).toString()));
                }
            }
        }
        sb.append("\n");
    }


    private static void add(String name, String type, int age, int position) {

        Player newPlayer = new Player(name, type, age);

        if (ranklist.isEmpty()) {
            ranklist.add(newPlayer);
        } else {

         //   int numberPositions = ranklist.size();
//            if (position > numberPositions) {
//                ranklist.add(newPlayer);
//            } else {
                ranklist.add(position-1, newPlayer);
//            }
        }


        if (!sortedByType.containsKey(type)) {
            sortedByType.put(type, new TreeSet<>());
        }


        sortedByType.get(type).add(newPlayer);


        sb.append(String.format("Added player %s to position %d\n", name, position));
    }


}