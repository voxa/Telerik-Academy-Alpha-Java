package HashMapTask_Telerik;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class UnitsOfWork {
    public static class Unit implements Comparable<Unit> {


        String name = "";
        private String type = "";
        private int attack = 0;


        private Unit(String name, String type, int attack) {
            this.name = name;
            this.type = type;
            this.attack = attack;
        }

        @Override
        public int compareTo(Unit currentUnit) {

            if (this.attack == currentUnit.attack) {
                return this.name.compareTo(currentUnit.name);
            } else if (this.attack > currentUnit.attack) {
                return -1;
            } else {
                return 1;
            }
        }

        String getType() {
            return type;
        }

        String getName() {
            return name;
        }

        int getAttack() {
            return attack;
        }

        @Override
        public String toString() {

            return String.format("%s[%s](%s)", getName(), getType(), getAttack());
        }
    }

    private static Map<String, Set<Unit>> unitsType = new HashMap<>();
    private static Map<String, Unit> units = new HashMap<>();

    public static void main(String[] args) throws IOException {


        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input = reader.readLine();

        unitsType.put("all", new TreeSet<>());

        String command = "";
        String name = "";
        String type = "";
        int attack = 0;
        int numUnits = 0;


        while (!input.equals("end")) {

            String[] line = input.split(" ");
            command = line[0];

            switch (command) {

                case "add":
                    name = line[1];
                    type = line[2];
                    attack = Integer.parseInt(line[3]);
                    addUnit(name, type, attack);
                    break;
                case "power":
                    numUnits = Integer.parseInt(line[1]);
                    powerUnit(numUnits);
                    break;
                case "find":
                    type = line[1];
                    findUnit(type);
                    break;
                case "remove":
                    name = line[1];
                    removeUnit(name);
                    break;
                default:
                    break;
            }
            input = reader.readLine();
        }
        System.exit(0);
    }

    private static void powerUnit(int numUnits) {

        System.out.print("RESULT: ");
        // tuk da stawa sortirowkata ?

        System.out.print(
                unitsType.get("all")
                        .stream()
                        .limit(numUnits)
                        .map(Unit::toString)
                        .collect(Collectors.joining(", ")));
        System.out.println();
    }


    private static void findUnit(String type) {

        System.out.print("RESULT: ");

        if (unitsType.containsKey(type)) {
            System.out.print(
                    unitsType
                            .get(type)
                            .stream()
                            .limit(10)
                            .map(Unit::toString)
                            .collect(Collectors.joining(", ")));

        }
        System.out.println();
    }


    private static void removeUnit(String name) {

        if (units.containsKey(name)) {

            Unit currentUnit = units.get(name);
            String type = currentUnit.getType();

            unitsType.get(type).remove(currentUnit);
            unitsType.get("all").remove(currentUnit);
            units.remove(name);

            System.out.println(String.format("SUCCESS: %s removed!", name));
        } else {
            System.out.println(String.format("FAIL: %s could not be found!", name));
        }
    }

    private static void addUnit(String name, String type, int attack) {

        Unit currentUnit = new Unit(name, type, attack);
// The putIfAbsent() method returns the previous value associated with the specified key, or null if there was no mapping for the key

        if(units.putIfAbsent(name, currentUnit)==null){

            if (!unitsType.containsKey(type)) {
                unitsType.put(type, new TreeSet<>());
            }
            unitsType.get(type).add(currentUnit);
            unitsType.get("all").add(currentUnit);

            System.out.println(String.format("SUCCESS: %s added!", name));
        } else {
            System.out.println(String.format("FAIL: %s already exists!", name));
        }
    }
}