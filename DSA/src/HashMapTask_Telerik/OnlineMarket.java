package HashMapTask_Telerik;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

public class OnlineMarket {


    static class Product implements Comparable<Product> {

        String name = "";
        String type = "";
        double price = 0.0;
       DecimalFormat df = new DecimalFormat("#.####");

        Product(String name, double price, String type) {
            this.name = name;
            this.type = type;
            this.price = price;
        }

        @Override
        public int compareTo(Product product) {
            int priceCompare = Double.compare(this.price, product.price);
            if (priceCompare != 0) {
                return priceCompare;
            }
            return this.name.compareTo(product.name);
        }

        @Override
        public String toString() {
            return String.format("%s(%s)", name, df.format(price));
        }

    }


    private static HashMap<String, Product> products = new HashMap<>();
    private static HashMap<String, TreeSet<Product>> allPrByType = new HashMap<>();
    private static TreeSet<Product> order = new TreeSet<>(); // tuk gi butame

    private static StringBuilder sb = new StringBuilder();

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();


        while (true) {

            String[] line = input.split(" ");
            String command = line[0];

            switch (command) {

                case "add":

                    String name = line[1];
                    double pricePr = Double.parseDouble(line[2]);
                    String type = line[3];

                    Product newPr = new Product(name, pricePr, type);

                    if (products.putIfAbsent(name, newPr) == null) {

                        allPrByType.putIfAbsent(type, new TreeSet<>());
                        allPrByType.get(type).add(newPr);
                        order.add(newPr);

                        sb.append(String.format("Ok: Product %s added successfully\n", name));
                    } else {
                        sb.append(String.format("Error: Product %s already exists\n", name));
                    }
                    break;

                case "filter":

                    String word = line[2];

                    if (word.equals("type")) {
                        String typeProduct = line[3];
                        filterByType(typeProduct);

                    } else if (word.equals("price")) {

                        String fromTo = line[3];
                        double pricePrFrom = Double.parseDouble(line[4]);

                        if (fromTo.equals("from")) {

                            if (line.length < 7) {

                                filterByPriceFrom(pricePrFrom);
                            } else {
                                double pricePrTo = Double.parseDouble(line[6]);
                                filterByPriceFromTo(pricePrFrom, pricePrTo);
                            }
                        } else { // ako e само to
                            double pricePrTo = Double.parseDouble(line[4]);
                            filterByPriceTo(pricePrTo);
                        }
                    }
                    break;
                case "end":
                    reader.close();
                    System.out.println(sb.toString());
                    System.exit(0);
                    return;
            }
            input = reader.readLine();
        }
    }


    private static void filterByType(String typeProduct) {

        if (allPrByType.containsKey(typeProduct)) {

            sb.append("Ok: ");

            int count = 0;
            String prefix = "";

            Set<Product> currentType = allPrByType.get(typeProduct);

            for (Product p : currentType) {

                if (count == 10) {
                    break;
                }
                sb.append(prefix);
                prefix = ", ";
                sb.append(p.toString());
                count++;
            }

            sb.append("\n");

        } else {
            sb.append(String.format("Error: Type %s does not exists\n", typeProduct));
        }
    }

    private static void filterByPriceFromTo(double pricePr, double pricePrTo) {

        sb.append("Ok: ");

        String prefix = "";
        int count = 0;

        Set<Product> allProducts = order;

        for (Product p : allProducts) {
            if (count == 10) {
                break;
            }
            if (p.price >= pricePr && p.price <= pricePrTo) {
                sb.append(prefix);
                prefix = ", ";
                sb.append(p.toString());
                count++;
            }
        }
        sb.append("\n");
    }

    private static void filterByPriceTo(double pricePrTo) {

        sb.append("Ok: ");

        int count = 0;
        String prefix = "";

        Set<Product> allProducts = order;

        for (Product p : allProducts) {

            if (count == 10) {
                break;
            }
            if (p.price <= pricePrTo) {
                sb.append(prefix);
                prefix = ", ";
                sb.append(p.toString());
                count++;
            }
        }
        sb.append("\n");
    }

    private static void filterByPriceFrom(double pricePrFrom) {

        sb.append("Ok: ");

        int count = 0;
        String prefix = "";

        Set<Product> allProducts = order;

        for (Product p : allProducts) {

            if (count == 10) {
                break;
            }
            if (p.price >= pricePrFrom) {
                sb.append(prefix);
                prefix = ", ";
                sb.append(p.toString());
                count++;
            }
        }
        sb.append("\n");
    }
}