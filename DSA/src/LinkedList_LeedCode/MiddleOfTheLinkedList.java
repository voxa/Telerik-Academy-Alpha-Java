package LinkedList_LeedCode;

public class MiddleOfTheLinkedList {


    public static void main(String[] args) {

//        [1,2,3,4,5]

        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);

        myPrint(middleOfTheLinkedList(head));
    }

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    public static class ListNode {

        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }


    private static void myPrint(ListNode x) {

        if (x != null) {
            System.out.println(x.val);

        }
    }


    private static ListNode middleOfTheLinkedList(ListNode head) {

        int size = 0;

        ListNode currentNode = head;

        while (currentNode.next != null) {
            size++;
            currentNode = currentNode.next;
        }
        size++;

        int position = 0;

        if (size % 2 == 0) {
            position = size / 2 + 1;
        } else {
            position = size / 2 + 1;
        }

        ListNode middleNode = head;

        while (position - 1 > 0) {

            middleNode = middleNode.next;
            position--;
        }


        return middleNode;

    }
}