package LinkedList_LeedCode;

public class RemoveDuplicatesFromSortedList {


    public static void main(String[] args) {

//        1->1->2->3->3->NULL
        // tuk go pulnq
        ListNode head = new ListNode(1);
        head.next = new ListNode(1);
        head.next.next = new ListNode(2);
        head.next.next.next = new ListNode(3);
        head.next.next.next.next = new ListNode(3);

        myPrint(deleteDuplicates(head));
    }

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    public static class ListNode {

        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }


    private static void myPrint(ListNode x) {

        while (x != null) {
            System.out.println(x.val);
            x = x.next;
        }
    }


    private static ListNode deleteDuplicates(ListNode head) {

        ListNode resultNode = head;

        if (head == null) {
            return head;
        }

        while (head.next != null) {

            if (head.val == head.next.val) {

                head.next = head.next.next;
            } else {

                head = head.next;

            }
        }
        return resultNode;

    }
}