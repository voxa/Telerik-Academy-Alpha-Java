package LinkedList_LeedCode;

public class PalindromeLinkedList {


    public static void main(String[] args) {



     //   1->2->3->4->5->NULL

        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(2);
        head.next.next.next = new ListNode(1);
      //  head.next.next.next.next = new ListNode(5);

        System.out.println(isPalindrome(head));

//        while (node != null) {
//            System.out.println(node.val);
//            node = node.next;
//        }

    }


    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    public static class ListNode {

        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }

    }

    private static boolean isPalindrome(ListNode head) {

        int size = 0;

        ListNode current = head;

        while (current != null) {
            current = current.next;
            size ++;
        }

        // Revert the first half nodes

        current = head;
        ListNode previous = null;

        for (int i = 0; i < size/2; i += 1) {

            ListNode tmp = current.next;
            current.next = previous;
            previous = current;
            current = tmp;
        }

        if (size % 2 == 1 && previous != null) {

            current = current.next;
        }

        while (previous != null) {

            if (previous.val != current.val) {
                return false;
            }
            previous = previous.next;
            current = current.next;

        }
        return true;
    }
}