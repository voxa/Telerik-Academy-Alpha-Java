package LinkedList_LeedCode;

public class RemoveLinkedListElements {


    public static void main(String[] args) {

// Input:  1->2->6->3->4->5->6, val = 6
//Output: 1->2->3->4->5


        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(6);
        head.next.next.next = new ListNode(3);
        head.next.next.next.next = new ListNode(4);

        ListNode node = removeElements(head, 6);

        while (node != null) {
            System.out.println(node.val);
            node = node.next;
        }

    }


    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    public static class ListNode {

        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }

    }

    private static ListNode removeElements(ListNode head, int val) {


        if (head == null) {
            return head;
        }
        ListNode node = new ListNode(0);
        node.next = head;
        ListNode curr = node;

        while (curr != null) {
            if (curr.next != null && curr.next.val == val) {
                curr.next = curr.next.next;
            } else {
                curr = curr.next;
            }
        }

        return node.next;
    }
}