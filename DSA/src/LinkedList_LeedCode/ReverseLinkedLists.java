package LinkedList_LeedCode;

public class ReverseLinkedLists {


    public static void main(String[] args) {

//        [1,2,3,4,5]

     //   1->2->3->4->5->NULL

        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);

        ListNode node = reverseLinkedList(head);

        while (node != null) {
            System.out.println(node.val);
            node = node.next;
        }

    }


    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    public static class ListNode {

        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }

    }

    private static ListNode  reverseLinkedList(ListNode head) {


        ListNode prev = null;
        ListNode current = head;
        ListNode nextElem =null;

        while (current != null){

            nextElem = current.next;
            current.next = prev;
            prev = current;
            current = nextElem;

        }

        return prev;
    }
}