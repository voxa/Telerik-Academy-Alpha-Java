package LinkedList_LeedCode;

public class MergeTwoSortedLists {


    public static void main(String[] args) {

//        [1,2,3,4,5]

        ListNode head1 = new ListNode(1);
        head1.next = new ListNode(2);
        head1.next.next = new ListNode(4);

        ListNode head2 = new ListNode(1);
        head2.next = new ListNode(3);
        head2.next.next = new ListNode(4);

        ListNode node = mergeTwoSortedLists(head1, head2);

        while (node != null) {
            System.out.println(node.val);
            node = node.next;
        }

    }

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    public static class ListNode {

        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }

    }


    private static ListNode  mergeTwoSortedLists(ListNode head1, ListNode head2) {


        ListNode result = new ListNode(-100);
        ListNode node = result;

        while (head1 != null && head2 != null) {

            if (head1.val< head2.val) {

                node.next = new ListNode(head1.val);
                head1 = head1.next;

            } else {

                node.next = new ListNode(head2.val);
                head2 = head2.next;
            }
            node = node.next;
        }

        while (head2 != null) {

            node.next = head2;
            head2 = head2.next;
            node = node.next;
        }

        while (head1 != null) {
            node.next = head1;
            head1 = head1.next;
            node = node.next;
        }

        return result.next;
    }
}