package StackTasks_LeedCode;

import java.util.Stack;

public class ValidParentheses {


    public static void main(String[] args) {

        String input = "{[]";

        System.out.println(isValid(input));
    }


    public static boolean isValid(String input) {

        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < input.length(); i++) {

            char currChar = input.charAt(i);

            if (stack.isEmpty()) {
                stack.push(currChar);

            } else {
                if ((stack.peek() == '(' && currChar == ')') ||
                        (stack.peek() == '{' && currChar == '}') ||
                        (stack.peek() == '[' && currChar == ']')) {

                    stack.pop();

                } else
                    stack.push(currChar);
            }
        }
        return stack.isEmpty();
    }

}