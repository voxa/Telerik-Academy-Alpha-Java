package StackTasks_LeedCode;

import java.util.*;

public class NextGreaterElement {

    public static void main(String[] args) {


        int[] nums1 = {1, 3, 5, 2, 4};
        int[] nums2 = {6, 5, 4, 3, 2, 1, 7};

        //Output: [7,7,7,7,7]

        int[] result = nextGreaterElement(nums1, nums2);

        System.out.println(Arrays.toString(result));
    }


    public static int[] nextGreaterElement(int[] nums1, int[] nums2) {

        Stack<Integer> stack = new Stack<>();


        ArrayList<Integer> keys = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();

        for (int num : nums2) {

            while (!stack.empty() && stack.peek() < num) {
                keys.add(stack.pop());
                values.add(num);

            }
            stack.push(num);
        }
        System.out.println(keys.toString());
        System.out.println(values.toString());


        int[] result = new int[nums1.length];

        for (int i = 0; i < nums1.length; i++) {

            int curNum = nums1[i];

            if (keys.contains(curNum)) {

                int index = keys.indexOf(curNum);
                result[i] = values.get(index);

            } else {
                result[i] = -1;
            }
        }
        return result;


    }
}