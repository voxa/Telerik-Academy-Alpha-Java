package StackTasks_LeedCode;

import java.util.Arrays;
import java.util.Stack;

public class AsteroidCollision {


    public static void main(String[] args) {

        //   int[] asteroids = {10, -5};
        //int[] asteroids = {5, 10, -5};
        // int[] asteroids = {8, -8};
        //  int[] asteroids = {10, 2, -5};
        //   int[] asteroids = {-2, -1, 1, 2};
        // int[] asteroids = {1, -2, -2, -2};
        //  int[] asteroids = {1, -1, -2, -2};
        // int[] asteroids = {10,2,-5};
        //  int[] asteroids = {10, 2, -50}; // !!!!
        //   int[] asteroids = {-2,1,-2};
        //  int[] asteroids = {5, 10, -5};
          int[] asteroids = {-2, 2, 1, -2};

     //   int[] asteroids = {1, 2, 1, -2};

        System.out.println(Arrays.toString(asteroidCollision(asteroids)));

    }

    public static int[] asteroidCollision(int[] asteroids) {

        Stack<Integer> stack = new Stack<>();


        for (int i = 0; i < asteroids.length; i++) {

            int current = asteroids[i];
           int flag = 0;

            if (stack.empty()) {

                stack.push(asteroids[i]);

            } else if (stack.peek() > 0 && current < 0) {

                if (stack.peek() == -current) {
                    stack.pop();
                    continue;
                }

                while (!stack.empty() && stack.peek() > 0 && current < 0) {

                    if (stack.peek() < -current) {
                        stack.pop();

                    } else if (stack.peek() == -current) {
                        stack.pop();
                        flag = -1;
                        break;
                    } else {
                        flag = -1;
                        current = 0;
                    }
                }

                if (flag == 0) {
                    stack.push(current);
                }

            } else {
                stack.push(current);
            }

        }

        int[] result = new int[stack.size()];

        for (int i = stack.size() - 1; i >= 0; i--) {
            result[i] = stack.pop();
        }

        return result;
    }
}