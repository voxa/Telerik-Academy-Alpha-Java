package StackTasks_LeedCode;

import java.util.*;


public class MyQueue {

    // Implement Queue Using Stack


    public static void main(String[] args) {


        Queue queue = new Queue();

        queue.push(1);
        queue.push(2);
        queue.push(3);
        queue.push(4);

        System.out.println("Queue => " + queue.value.toString());
        System.out.println();

        System.out.println(queue.pop());

        System.out.println("Queue => " + queue.value.toString());
        System.out.println();

        System.out.println(queue.peek());

        System.out.println("Queue => " + queue.value.toString());
        System.out.println();

        System.out.println(queue.empty());

        System.out.println("Queue => " + queue.value.toString());
        System.out.println();

    }

    static class Queue {

        Stack<Integer> value = new Stack<Integer>();
        Stack<Integer> temp = new Stack<Integer>();


        /**
         * Push element x to the back of queue.
         */

        public void push(int x) {

            if (value.isEmpty()) {
                value.push(x);
            } else {

                while (!value.isEmpty()) {
                    temp.push(value.pop());
                }
                value.push(x);

                while (!temp.isEmpty()) {
                    value.push(temp.pop());
                }
            }
        }

        /**
         * Removes the element from in front of queue and returns that element.
         */
        public int pop() {
            return value.pop();
        }

        /**
         * Get the front element.
         */
        public int peek() {
            return value.peek();
        }

        /**
         * Returns whether the queue is empty.
         */
        public boolean empty() {
            return value.isEmpty();
        }


    }
}