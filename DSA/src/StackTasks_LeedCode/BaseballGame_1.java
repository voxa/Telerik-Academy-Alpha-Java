package StackTasks_LeedCode;

import java.util.Stack;

public class BaseballGame_1 {


    public static void main(String[] args) {

        String[] ops = {"5", "-2", "4", "C", "D", "9", "+", "+"};
        // "5","-2","4","C","D","9","+","+"  Output: 27
        //  {"5", "2", "C", "D", "+"}; Output: 30

        System.out.println(calPoints(ops));
    }

    public static int calPoints(String[] ops) {

        Stack<Integer> points = new Stack<>();

        int sum = 0;
        int lastPoint = 0;

        for (int i = 0; i < ops.length; i++) {

            String currCommand = ops[i];

            if (currCommand.equals("C")) {

                if (!points.empty()) {
                    lastPoint = points.pop();
                    sum = sum - lastPoint;

                }

            } else if (currCommand.equals("D")) {

                if (!points.empty()) {
                    lastPoint = 2 * points.peek();
                    sum = sum + lastPoint;
                    points.push(lastPoint);

                }

            } else if (currCommand.equals("+")) {

                if (!points.empty()) {
                    lastPoint = points.peek();
                    sum = sum + lastPoint;
                    points.pop();
                }
                int previousPoint = 0;
                if (!points.empty()) {
                    previousPoint = points.peek();
                    sum = sum + previousPoint;

                }
                points.push(lastPoint);
                points.push(lastPoint + previousPoint);
            } else {
                sum = sum + points.push(Integer.parseInt(currCommand));

            }
        }

        return sum;
    }
}