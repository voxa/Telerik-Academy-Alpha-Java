package StackTasks_LeedCode;

import java.util.Stack;

public class BackspaceStringCompare {


    public static void main(String[] args) {

        String input1 = "ab#c";
        String input2 = "ad#c";

        System.out.println(isEqual(input1, input2));
    }


    public static boolean isEqual(String input1, String input2) {

        Stack<Character> stack1 = new Stack<>();
        Stack<Character> stack2 = new Stack<>();

        for (int i = 0; i < input1.length(); i++) {

            char currChar = input1.charAt(i);

            if(currChar != '#') {
                stack1.push(currChar);
            }
            if (currChar == '#' && !stack1.empty() ) {
                stack1.pop();
            }
        }
        for (int i = 0; i < input2.length(); i++) {

            char currChar = input2.charAt(i);

            if(currChar != '#') {
                stack2.push(currChar);
            }
            if (currChar == '#' && !stack2.empty() ) {
                stack2.pop();
            }
        }
        return stack1.equals(stack2);
    }
}