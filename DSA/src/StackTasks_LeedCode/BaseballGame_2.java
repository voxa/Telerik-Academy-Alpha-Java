package StackTasks_LeedCode;

import java.util.Stack;

public class BaseballGame_2 {


    public static void main(String[] args) {

        String[] ops = {"5", "-2", "4", "C", "D", "9", "+", "+"};
        // "5","-2","4","C","D","9","+","+"  Output: 27
        //  {"5", "2", "C", "D", "+"}; Output: 30

        System.out.println(calPoints(ops));
    }

    public static int calPoints(String[] ops) {

        Stack<Integer> points = new Stack<>();

        int sum = 0;

        for (int i = 0; i < ops.length; i++) {

            String str = ops[i];

            switch (str) {
                case "C":
                    points.pop();
                    break;
                case "D":
                    points.push(2*points.peek());
                    break;
                case "+":
                    int lastValid = points.peek();
                    points.pop();
                    int lastPush = points.peek() + lastValid;
                    points.push(lastValid);
                    points.push(lastPush);
                    break;

                default:
                    points.push(Integer.parseInt(str));
                    break;
            }
        }

        for ( int point: points  ) {
            sum += point;
                    }

        return sum;
}
}