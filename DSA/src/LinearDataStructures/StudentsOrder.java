package LinearDataStructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StudentsOrder {

    public static void main(String[] args) throws IOException {

        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

        String[] inputLineOne = br.readLine().split(" ");

        int n = Integer.parseInt(inputLineOne[0]);
        int m = Integer.parseInt(inputLineOne[1]);

        String [] names = br.readLine().split(" ");
        List<String> result = new ArrayList<>();
        Collections.addAll(result, names);


        for (int i = 0; i < m; i++) {

            String [] namesPair = br.readLine().split(" ");

            String firstStudent = namesPair [0];
            String secondStudent = namesPair [1];

           int oldPosition = result.indexOf(firstStudent);
          int  newPosition = result.indexOf(secondStudent);

            if (oldPosition == newPosition - 1) {
                continue;
            }
            result.add(newPosition, firstStudent);

            if (oldPosition < newPosition) {
                result.remove(oldPosition);
            } else {
                result.remove(oldPosition + 1);
            }


        }
        StringBuilder sb = new StringBuilder();
        for (String s: result) {
            sb.append(s);
            sb.append(" ");
        }
        System.out.println(sb.toString().trim());
    }
}
