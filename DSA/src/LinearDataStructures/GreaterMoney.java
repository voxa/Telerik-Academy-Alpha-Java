package LinearDataStructures;


import java.util.Scanner;

public class GreaterMoney {

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        String[] bag1 = sc.nextLine().split(",");
        String[] bag2 = sc.nextLine().split(",");

        int[] nums1 = new int[bag1.length];
        int[] nums2 = new int[bag2.length];

        for (int i = 0; i < bag1.length; i++) {

            int num1 = Integer.parseInt(bag1[i]);
            nums1[i] = num1;
        }

        for (int i = 0; i < bag2.length; i++) {
            int num2 = Integer.parseInt(bag2[i]);
            nums2[i] = num2;
        }

        String result = "";

        for (int i = 0; i < nums1.length; i++) {
           int  nextNum = findNextBigger(nums1[i], nums2);
            result = result + nextNum + ",";

        }


        System.out.println(result.substring(0, result.length() - 1));

    }

    private static int  findNextBigger(int num, int[] bag2) {

        int index = -1;

        for (int i = 0; i < bag2.length; i++) {
            int nextNum = bag2[i];
            if (nextNum == num) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            return -1;
        } else {
            for (int i = index; i < bag2.length; i++) {
                if (bag2[i] > num) {
                    return  bag2[i];
                }
            }
        }
        return -1;
    }
}
