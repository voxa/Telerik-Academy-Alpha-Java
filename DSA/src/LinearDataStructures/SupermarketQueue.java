package LinearDataStructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SupermarketQueue {

    private static List<String> list = new ArrayList<>();
    private static Map<String, Integer> uniqueNames = new HashMap<>();

    private static StringBuilder sb = new StringBuilder();

    public static void main(String[] args) throws IOException {



        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        String command = "";
        String name = "";
        int position = 0;
        int count = 0;

        while (!input.equals("End")) {

            String[] line = input.split(" ");
            command = line[0];

            switch (command) {

                case "Append":
                    name = line[1];
                    add(name);
                    break;
                case "Find":
                    name = line[1];
                    find(name);
                    break;
                case "Serve":
                    count = Integer.parseInt(line[1]);
                    serve(count);
                    break;
                case "Insert":
                    position = Integer.parseInt(line[1]);
                    name = line[2];
                    insert(position, name);
                    break;
                default:
                    break;
            }
            input = reader.readLine();
        }
        reader.close(); // !!!!!!
        System.out.println(sb.toString());
    }


    private static void serve(int count) {

        if (list.size() >= count) {

            while (count > 0) {

                String nameInQ = list.get(0);

                sb.append(nameInQ);
                sb.append(" ");


                if (uniqueNames.get(nameInQ) == 0) {
                    uniqueNames.remove(nameInQ);
                } else {
                    uniqueNames.put(nameInQ, uniqueNames.get(nameInQ) - 1);
                }

                list.remove(0);
                count--;
            }

        } else {
            sb.append("Error\n");
            return;
        }
        sb.append("\n");
    }

    private static void find(String name) {

        if (uniqueNames.containsKey(name)) {
            sb.append((uniqueNames.get(name) + 1));
            sb.append("\n");
        } else {
            sb.append("0\n");
        }

    }

    private static void add(String name) {

        list.add(name);
        sb.append("OK\n");

        addNameCount(name);
    }


    private static void insert(int position, String name) {

        if (position >= 0 && position <= list.size()) {

            list.add(position, name);
            sb.append("OK\n");

            addNameCount(name);

        } else {
            sb.append("Error\n");
        }

    }

    private static void addNameCount(String name) {

        if (uniqueNames.putIfAbsent(name, 0) != null) {
            uniqueNames.put(name, uniqueNames.get(name) + 1);
        }
    }

}

