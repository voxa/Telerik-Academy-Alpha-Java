package BinarySearchTreeImpl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinarySearchTreeImpl implements BinarySearchTree {

    private BinaryTreeNode root;
    private List<Integer> list = new ArrayList<>();


    public BinarySearchTreeImpl() {
        this.root = null;
    }


    @Override
    public BinaryTreeNode getRoot() {
        return root;
    }

    @Override
    public void insert(int value) {

        BinaryTreeNode newNode = new BinaryTreeNode(value);
        if (root == null) {
            root = newNode;
            return;
        }

        BinaryTreeNode current = root;
        BinaryTreeNode parent;
        while (true) {
            parent = current;
            if (value < current.getValue()) {
                current = current.leftChild;
                if (current == null) {
                    parent.leftChild = newNode;
                    return;
                }
            } else {
                current = current.rightChild;
                if (current == null) {
                    parent.rightChild = newNode;
                    return;
                }

            }
        }
    }


    @Override
    public BinaryTreeNode search(int value) {

        BinaryTreeNode current = root;

        while (current != null) {

            if (current.getValue() == value) {
                return current;
            } else if (current.getValue() > value) {
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }
        }
        return null;

    }

    @Override
    public List<Integer> inOrder() {

        if (root == null) {
            return list;
        } else {
            inOrder(root);
        }
        return list;
    }

    private void inOrder(BinaryTreeNode root) {

        if (root.leftChild != null) {
            inOrder(root.leftChild);
        }
        list.add(root.getValue());
        if (root.rightChild != null) {
            inOrder(root.rightChild);
        }

    }


    @Override
    public List<Integer> postOrder() {

        if (root == null) {
            return list;
        } else {
            postOrder(root);
        }
        return list;
    }

    private void postOrder(BinaryTreeNode root) {

        if (root.leftChild != null) {
            postOrder(root.leftChild);
        }

        if (root.rightChild != null) {
            postOrder(root.rightChild);
        }
        list.add(root.getValue());
    }

    @Override
    public List<Integer> preOrder() {

        if (root == null) {
            return list;
        } else {
            preOrder(root);
        }
        return list;
    }

    private void preOrder(BinaryTreeNode root) {

        list.add(root.getValue());
        if (root.leftChild != null) {
            preOrder(root.leftChild);
        }

        if (root.rightChild != null) {
            preOrder(root.rightChild);
        }

    }

    @Override
    public List<Integer> bfs() {

        if (root == null) {
            return list;
        } else {

            Queue<BinaryTreeNode> queue = new LinkedList<>();
            queue.add(root);

            while (!queue.isEmpty()) {

                BinaryTreeNode tempNode = queue.poll();
                list.add(tempNode.getValue());

                if (tempNode.leftChild != null) {
                    queue.add(tempNode.leftChild);
                }

                if (tempNode.rightChild != null) {
                    queue.add(tempNode.rightChild);
                }
            }
            return list;
        }
    }


    @Override
    public int height() {

        if (root == null) {
            return -1;
        } else {

            return getBinaryTreeNodeHeight(this.root);
        }
    }

    private int getBinaryTreeNodeHeight(BinaryTreeNode current) {

        if (current == null) {
            return -1;
        }

        return Math.max(getBinaryTreeNodeHeight(current.getLeftChild()),
                getBinaryTreeNodeHeight(current.getRightChild())) + 1;
    }


    @Override
    public BinaryTreeNode remove(int value) {



        if (root == null) {
            return null;
        }

        if (root.getValue() == value) {
            return root;
        }

        if (search(value) == null) {
            return null;
        }

        root = deleteNode(root, value);

        if (root != null) {

                       return new BinaryTreeNode(value) ;
        }

        return null;

    }

    private static BinaryTreeNode deleteNode(BinaryTreeNode current, int value) {

        if (current == null)
            return null;

        if (current.getValue() > value) {

            current.leftChild = deleteNode(current.leftChild, value);

        } else if (current.getValue() < value) {

            current.rightChild = deleteNode(current.rightChild, value);

        } else {

            if (current.leftChild != null && current.rightChild != null) {

                BinaryTreeNode minRight = minElement(current.rightChild);

                current.setValue(minRight.getValue());

                deleteNode(current.rightChild, minRight.getValue());

            } else if (current.leftChild != null) {

                current = current.leftChild;

            } else if (current.rightChild != null) {

                current = current.rightChild;

            } else {

                return null;
            }
        }
        return current;
    }

    private static BinaryTreeNode minElement(BinaryTreeNode current) {
        if (current.leftChild == null) {
            return current;
        } else {
            return minElement(current.leftChild);
        }
    }

}
