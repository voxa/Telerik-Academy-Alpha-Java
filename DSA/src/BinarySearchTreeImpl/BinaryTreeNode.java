package BinarySearchTreeImpl;

public class BinaryTreeNode {


    private int value;
    BinaryTreeNode leftChild;
    BinaryTreeNode rightChild;


    public BinaryTreeNode() {
        this.value = 0;
        this.leftChild = null;
        this.rightChild = null;
    }

    public void setLeftChild(BinaryTreeNode leftChild) {
        this.leftChild = leftChild;
    }

    public void setRightChild(BinaryTreeNode rightChild) {
        this.rightChild = rightChild;
    }

     BinaryTreeNode(int value) {
        this.value = value;
        this.leftChild = null;
        this.rightChild = null;
    }
    void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public BinaryTreeNode getLeftChild() {
        return leftChild;
    }

    public BinaryTreeNode getRightChild() {
        return rightChild;
    }
}
