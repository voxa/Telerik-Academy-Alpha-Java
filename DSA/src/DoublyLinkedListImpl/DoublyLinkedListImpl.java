package DoublyLinkedListImpl;

import java.util.*;


public class DoublyLinkedListImpl<T> implements DoublyLinkedList<T> {


    private DoubleNode<T> head = null;
    private DoubleNode<T> tail = null;
    private int count = 0;

// Implement all interfaces

    public DoublyLinkedListImpl() {
    }

    public DoublyLinkedListImpl(List<T> asList) {

        for (T element : asList) {
            addLast(element);
        }
    }

    @Override
    public DoubleNode getHead() {
        return head;
    }

    @Override
    public DoubleNode getTail() {
        return tail;
    }

    @Override
    public int getCount() {
        return count;
    }


    @Override
    public void addFirst(T value) {

        DoubleNode<T> newHead = new DoubleNode<>(value);

        if (this.count == 0) {

            head = newHead;
            tail = newHead;

        } else {

            newHead.next = head;
            head.prev = newHead;
            head = newHead;
            newHead.prev = null;
        }
        this.count++;
    }


    @Override
    public void addLast(T value) {

        DoubleNode<T> newTail = new DoubleNode<>(value);

        if (this.count == 0) {

            this.head = newTail;
            this.tail = newTail;

        } else {

            this.tail.next = newTail;
            newTail.prev = this.tail;
            this.tail = newTail;
            newTail.next = null;
        }
        count++;
    }


    @Override
    public void insertBefore(DoubleNode node, T value) {

        if (node == null) {
            throw new NullPointerException();
        }

        DoubleNode<T> newNode = new DoubleNode<>(value);
        DoubleNode previous = node.prev;

        count++;

        if (previous == null) {

            this.head.prev = newNode;
            newNode.next = this.head;
            this.head = newNode;

        } else {

            node.prev = newNode;
            previous.next = newNode;
            newNode.next = node;
            newNode.prev = previous;
        }
    }


    @Override
    public void insertAfter(DoubleNode node, T value) {

        DoubleNode<T> newNode = new DoubleNode<T>(value);

        if (node == null) {
            throw new NullPointerException();
        }
        count++;

        if (node.next == null) {

            this.tail.next = newNode;
            newNode.prev = this.tail;
            this.tail = newNode;

        } else {

            newNode.next = node.next;
            node.next.prev = newNode;
            node.next = newNode;
            newNode.prev = node;
        }
    }


    @Override
    public T removeFirst() {

        if (this.count == 0) {
            throw new NullPointerException();
        }

        T element = this.head.value;
        DoubleNode<T> newFirst = this.head.next;
        count--;

        if (count == 0) {
            this.head = null;
            this.tail = null;
            return element;
        }

        newFirst.prev = null;
        this.head = newFirst;
        return element;
    }


    @Override
    public T removeLast() {

        if (this.count == 0) {
            throw new NullPointerException();
        }
        T element = this.tail.value;
        DoubleNode<T> newTail = this.tail.prev;
        count--;

        if (count == 0) {
            this.head = null;
            this.tail = head;
            return element;
        }

        newTail.next = null;
        this.tail = newTail;

        return element;
    }

    public DoubleNode find(T value) {

        DoubleNode<T> findNode = head;

        while (findNode != null) {

            if (findNode.getValue().equals(value)) {

                return findNode;
            }
            findNode = findNode.getNext();
        }
        return null;
    }


    @Override
    public Iterator<T> iterator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<T> {

        DoubleNode<T> currentNode = head;

        @Override
        public boolean hasNext() {
            return (currentNode != null);
        }

        @Override
        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            T element = currentNode.value;
            currentNode = currentNode.next;
            return element;
        }
    }
}



