package StackTask_Telerik;

import java.util.Scanner;
import java.util.Stack;

public class BracketsExpressions {

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        String input = sc.nextLine();

        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < input.length(); i++) {

            char currChar = input.charAt(i);

            if (currChar == '(') {
                stack.push(i);
            }
            if (currChar == ')') {

                System.out.println(input.substring(stack.pop(), i +1  ));

            }
        }
    }
}
