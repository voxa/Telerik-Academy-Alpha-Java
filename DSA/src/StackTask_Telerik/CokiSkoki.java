package StackTask_Telerik;

import java.util.Scanner;

public class CokiSkoki {

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        int n = Integer.parseInt(sc.nextLine());

        String[] input = sc.nextLine().split(" ");

        int[] nums = new int[n];

        int maxH = 0;

        for (int i = 0; i < n; i++) {

            nums[i] = Integer.parseInt(input[i]);

            if (nums[i] > maxH) {
                maxH = nums[i];
            }
        }

        int jumps = 0;
        int numCurrent = 0;
        int nextPossition = 0;
        int max = 0;

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < n - 1; i++) {

            numCurrent = nums[i];

            if (numCurrent == maxH) {
                sb.append("0");
                sb.append(" ");
                continue;
            }

            for (int j = i + 1; j < nums.length; j++) {

                nextPossition = nums[j];

                if (nextPossition == maxH) {

                    jumps++; break;

                } else if (nextPossition > numCurrent) {

                    jumps++;
                    numCurrent = nextPossition;
                }
            }

            sb.append(jumps);
            sb.append(" ");

            if (jumps > max) {
                max = jumps;
            }

            jumps = 0;
        }

        sb.append("0");

        System.out.println(max);
        System.out.println(sb.toString());
    }
}




