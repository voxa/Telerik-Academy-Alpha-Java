package StackTask_Telerik;

import java.util.Scanner;
import java.util.Stack;

public class HDNL_Toy {

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        int n = Integer.parseInt(sc.nextLine());

        Stack<String> stack = new Stack<>();

        String input = "";
        int num = 0;
        int peekNum = 0;

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < n; i++) {

            input = sc.nextLine();
            num = Integer.parseInt(input.replaceAll("[a-z]", ""));

            if (stack.isEmpty()) {
                System.out.println("<" + stack.push(input) + ">");
                continue;
            }

            peekNum = Integer.parseInt(stack.peek().replaceAll("[a-z]", ""));

            if (peekNum < num) {

                sb.append(" ");

                System.out.println(sb + "<" + stack.push(input) + ">");

            } else if (peekNum == num) {

                System.out.println(sb + "</" + (stack.pop()) + ">");
                System.out.println(sb + "<" + stack.push(input) + ">");

            } else {

                while (!stack.isEmpty() && peekNum >= num) {

                    System.out.println(sb + "</" + (stack.pop()) + ">");

                    if (!stack.isEmpty()) {

                        peekNum = Integer.parseInt(stack.peek().replaceAll("[a-z]", ""));

                        if (peekNum >= num) {
                            sb.deleteCharAt(0);
                        }
                    }
                }

                System.out.println(sb + "<" + stack.push(input) + ">");

            }
        }

        while (!stack.isEmpty()) {

            System.out.println(sb + "</" + stack.pop() + ">");

            if (!stack.isEmpty()) {
                sb.deleteCharAt(0);
            }
        }


    }
}

